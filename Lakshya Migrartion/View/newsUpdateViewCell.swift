//
//  newsUpdateViewCell.swift
//  Lakshya Migrartion
//
//  Created by Raunak Singh on 7/1/18.
//  Copyright © 2018 Raunak Singh. All rights reserved.
//

import UIKit

class newsUpdateViewCell: UITableViewCell {


    @IBOutlet weak var titleLabel: UILabel!
    
    func updateUI(update: Updates) {
        titleLabel.text = update.title
    }
}
