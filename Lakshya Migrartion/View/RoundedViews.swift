//
//  RoundedViews.swift
//  Lakshya Migrartion
//
//  Created by Raunak Singh on 17/12/17.
//  Copyright © 2017 Raunak Singh. All rights reserved.
//

import UIKit

@IBDesignable
class RoundedViews: UIView {
    
    @IBInspectable
    
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = 10.0
        }

}
