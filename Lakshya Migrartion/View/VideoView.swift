//
//  VideoView.swift
//  Lakshya Migrartion
//
//  Created by Raunak Singh on 26/12/17.
//  Copyright © 2017 Raunak Singh. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class VideoView: UITableViewCell {
    

    @IBOutlet weak var videoImg: UIImageView!
    
    @IBOutlet weak var videoTitle: UILabel!
    
   
    func updateUI(vid: videos) {
        
        self.videoTitle.text = vid.videoTitle
        
        // downloading and setting the image for each video.
        
        let imageURL = URL(string: vid.videoImageURL)
        
        Alamofire.request(imageURL!).responseImage { Result in
            if let image = Result.value {
                self.videoImg.image = image
            }
        }
        
    }
    
    
    
}

