//
//  VisaClass.swift
//  Lakshya Migrartion
//
//  Created by Raunak Singh on 10/12/17.
//  Copyright © 2017 Raunak Singh. All rights reserved.
//

import Foundation

class VisaClass {                       //This is the class for 6 buttons.
   
    private var _visaType: String!
    private var _visaDesc: String!
    private var _vidLink: String!
    
    
    var visaType: String {
        if _visaType == nil {
            _visaType = ""
        }
        return _visaType
    }
    var visaDesc: String {
        if _visaDesc == nil {
        _visaDesc = ""
        }
        return _visaDesc
    }
    var vidLink: String {
        if _vidLink == nil {
            _vidLink = ""
        }
        return _vidLink
    }
    
    init(vt: String, vd: String, vl: String) {
        _visaType = vt
        _visaDesc = vd
        _vidLink = vl
    }
    
}

