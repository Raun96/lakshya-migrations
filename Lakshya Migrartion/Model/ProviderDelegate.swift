//
//  ProviderDelegate.swift
//  Lakshya Migrartion
//
//  Created by Raunak Singh on 3/1/18.
//  Copyright © 2018 Raunak Singh. All rights reserved.
//

import Foundation
import CallKit

class ProviderDelegate: NSObject {
    let provider: CXProvider
    let callManager: CXCallDirectoryManager
    
    init(callMngr: CXCallDirectoryManager) {
        self.callManager = callMngr
          super.init()
        provider = CXProvider(configuration: self.providerConfiguration)
      
        provider.setDelegate(self as? CXProviderDelegate, queue: nil)
    }
    
    var providerConfiguration: CXProviderConfiguration {
        let providerConfig = CXProviderConfiguration(localizedName: "Lakshya Migration")
        providerConfig.supportedHandleTypes = [.phoneNumber]
        providerConfig.maximumCallsPerCallGroup = 1
        
        return providerConfig
    }
}
