//
//  Updates.swift
//  Lakshya Migrartion
//
//  Created by Raunak Singh on 7/1/18.
//  Copyright © 2018 Raunak Singh. All rights reserved.
//

import Foundation

class Updates {
    private var _link: String!
    private var _title: String!
    
    var link: String {
        if _link == nil {
            _link = ""
        }
        return _link
    }
    
    var title: String {
        if _title == nil {
            _title = ""
        }
        return _title
    }
    
    init(title: String, link: String) {
        self._link = link
        self._title = title
    }
    
    
}
