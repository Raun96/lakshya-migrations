//
//  InsetTextField.swift
//  Lakshya Migrartion
//
//  Created by Raunak Singh on 5/3/18.
//  Copyright © 2018 Raunak Singh. All rights reserved.
//

import UIKit

class InsetTextField: UITextField {
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
    }

  let padding = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }


}
