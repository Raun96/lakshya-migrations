//
//  Client.swift
//  Lakshya Migrartion
//
//  Created by Raunak Singh on 13/1/18.
//  Copyright © 2018 Raunak Singh. All rights reserved.
//

import Foundation

class Client {
    private var _fName: String!
    private var _lName: String!
    private var _number: String!
    private var _email: String!
    private var _visaType: String!
    private var _aboutUs: String!
    private var _description: String!
    
    var fName: String {
        if _fName == nil {
            _fName = ""
        }
        return _fName
    }
    
    var lName: String {
        if _lName == nil {
            _lName = ""
        }
        return _lName
    }
    
    var number: String {
        if _number == nil {
            _number = ""
        }
        return _number
    }
    
    var email: String {
        if _email == nil {
            _email = ""
        }
        return _email
    }
    
    var aboutUs: String {
        get {
        if _aboutUs == nil {
            _aboutUs = ""
        }
        return _aboutUs
            
        }
        set(newVal) {
            _aboutUs = newVal
        }
    }
    
    var description: String {
        get {
        if _description == nil {
            _description = ""
        }
        return _description
        }
        set(newVal) {
            _description = newVal
        }
    }
    
    var visaType: String {
        if _visaType == nil {
            _visaType = ""
        }
        return _visaType
    }
    
    init(fn: String, ln: String, num: String, email: String, vt: String) {
        self._fName = fn
        self._lName = ln
        self._number = num
        self._email = email
        self._visaType = vt
    }
    
    
}
