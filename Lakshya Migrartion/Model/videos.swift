//
//  videos.swift
//  Lakshya Migrartion
//
//  Created by Raunak Singh on 24/12/17.
//  Copyright © 2017 Raunak Singh. All rights reserved.
//

import Foundation

class videos {
  
    
    private var _videoTitle: String!
    private var _videoURL: String!
    private var _videoImageURL: String!
    private var _description: String!
    private var _date: String!
    
    var videoTitle : String {
        if _videoTitle == nil{
        _videoTitle = ""
    }
        return _videoTitle
    }
    
    var videoURL : String {
        if _videoURL == nil {
            _videoURL = ""
        }
        return _videoURL
    }
    
    var videoImageURL: String {
        if _videoImageURL == nil {
            _videoImageURL = ""
        }
        return _videoImageURL
    }
    var description: String {
        if _description == nil {
            _description = ""
        }
        return _description
    }
    var date: String {
        if _date == nil {
            _date = ""
        }
        return _date
    }

    init(titl: String, vid: String, img: String, desc: String, date: String) {
        self._videoTitle = titl
        self._videoImageURL = img
        self._videoURL = vid
        self._description = desc
        self._date = date
    }
    

    
   
}
