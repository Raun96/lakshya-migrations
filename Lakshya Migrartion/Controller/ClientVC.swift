//
//  ClientVC.swift
//  Lakshya Migrartion
//
//  Created by Raunak Singh on 14/1/18.
//  Copyright © 2018 Raunak Singh. All rights reserved.
//

import UIKit

class ClientVC: UIViewController, UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate {

    
    @IBOutlet weak var visaTypeBtn: UIButton!
  
    @IBOutlet weak var visaStack: UIStackView!
    
    @IBOutlet weak var emailStack: UIStackView!
    var vtArray = ["Student Visa","Skilled Migration","Course Change","Employer Sponsorship","Partner Visa", "485 Visa"]
    var client: Client!

    @IBOutlet weak var pickerView: UIPickerView!
    
    @IBOutlet weak var fnameTF: UITextField!
    @IBOutlet weak var lnameTF: UITextField!
    @IBOutlet weak var contactTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
        pickerView.dataSource = self
        pickerView.delegate = self
        fnameTF.delegate = self
        lnameTF.delegate = self
        emailTF.delegate = self
        contactTF.delegate = self
        visaTypeBtn.layer.cornerRadius = 10.0
        pickerView.layer.cornerRadius = 10.0
        
        
        
    }

   
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return vtArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return vtArray[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        visaTypeBtn.setTitle(vtArray[row], for: UIControlState.normal)
        visaTypeBtn.setTitleColor(UIColor.red, for: UIControlState.normal)

        pickerView.isHidden = true
        
    }
    
    @IBAction func visaTypeBtnPressed(_ sender: Any) {

        pickerView.isHidden = false
    }
    @IBAction func backBtnPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
   
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        fnameTF.resignFirstResponder()
        lnameTF.resignFirstResponder()
        contactTF.resignFirstResponder()
        emailTF.resignFirstResponder()
        return (true)
        
    }
    
    func updateClientObject() {
        
        self.client = Client(fn: fnameTF.text!, ln: lnameTF.text!, num: contactTF.text!, email: emailTF.text!, vt: "\(visaTypeBtn.titleLabel!.text)")
        print(self.client.fName)
    }
    
    @IBAction func nextBtnPressed(_ sender: Any) {
        updateClientObject()
        performSegue(withIdentifier: "gotoclientVC2", sender: self.client)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "gotoclientVC2" {
            if let clientvc2 = segue.destination as? Client2VC {
                if let client = sender as? Client {
                    print(client)
                    clientvc2.client = client
                }
                
            }
        }
    }
    
}
