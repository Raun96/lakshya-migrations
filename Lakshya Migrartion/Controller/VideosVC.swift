//
//  VideosVC.swift
//  Lakshya Migrartion
//
//  Created by Raunak Singh on 17/12/17.
//  Copyright © 2017 Raunak Singh. All rights reserved.
//

import UIKit
import Alamofire

class VideosVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
   var videoArray = [videos]()
   var filteredvideoArray = [videos]()
   

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    var searchMode = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        EZLoadingActivity.show("Loading Videos...", disableUI: false)           //shows the loading view activity.
        
        tableView.dataSource = self
        tableView.delegate = self
        searchBar.delegate = self
        //To cancel keyboard with a tap anywhere on the screen. And still registering taps on TableViews and CollectionViews.
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:)))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)

        
        self.downloadVideoData {}
        
    }
    
    func downloadVideoData(completed : @escaping downloadComplete)  {           //function to download data
        let videodataURL = URL(string: jsonvideoURL)!
        Alamofire.request(videodataURL).responseJSON { Result in
            //print(Result)
            if let dict = Result.value as? Dictionary<String,AnyObject> {
                if let vid = dict["Videos"] as? [Dictionary<String,AnyObject>]{
                    
                    for obj in vid {
                        let title = obj["Title"] as? String
                        let img = obj["imageURL"] as? String
                        let vid = obj["videoURL"] as? String
                        let date = obj["date"] as? String
                        let desc = obj["description"] as? String
                        let v = videos(titl: title!, vid: vid!, img: img!, desc: desc!, date: date!)
                        self.videoArray.append(v)
                        print(self.videoArray.count)
                        print(self.videoArray)
                    }
                    EZLoadingActivity.hide()                            //Hides the loading view activity.
                    self.tableView.reloadData()
                    
                }
                
            }
        }
        completed()
    
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchMode{
            return filteredvideoArray.count
        } else {
            return videoArray.count
        }
    }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "vid", for: indexPath) as? VideoView {
            if searchMode == false {
            let vid = self.videoArray[indexPath.row]
            cell.updateUI(vid: vid)
            return cell
                
            } else {
            
                let vid = self.filteredvideoArray[indexPath.row]
                cell.updateUI(vid: vid)
                return cell
                
            }
        }
        else {
            return UITableViewCell()
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if searchMode == false {
        let vid = self.videoArray[indexPath.row]
        print(vid.videoURL, vid.videoTitle)
        performSegue(withIdentifier: "playVideo", sender: vid)
        } else {
            let vid = self.filteredvideoArray[indexPath.row]
            print(vid.videoURL, vid.videoTitle)
            performSegue(withIdentifier: "playVideo", sender: vid)
        }
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "playVideo" {
            if let PlayVideoVC = segue.destination as? PlayVideoVC {
                if let video = sender as? videos {
                    PlayVideoVC.vid = video
                    
                }
            }
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == nil || searchBar.text == "" {
            searchMode = false
            tableView.reloadData()
        } else {
            searchMode = true
            let searchText = searchBar.text
            
            filteredvideoArray = videoArray.filter({$0.videoTitle.range(of: searchText!) != nil})
            tableView.reloadData()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.endEditing(true)
        
    }
    
}
