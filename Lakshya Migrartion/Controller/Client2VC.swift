//
//  Client2VC.swift
//  Lakshya Migrartion
//
//  Created by Raunak Singh on 26/1/18.
//  Copyright © 2018 Raunak Singh. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class Client2VC: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource,UITextFieldDelegate{
    
    var client: Client!

    let findingSourceArray: [String] = ["Social Media","Family/Friends","Internet","Advertisement", "Others"]
   
    @IBOutlet weak var optionsBtn: UIButton!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var descriptionTF: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        pickerView.delegate = self
        pickerView.dataSource = self
        optionsBtn.layer.cornerRadius = 10.0
        pickerView.layer.cornerRadius = 10.0
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return findingSourceArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return findingSourceArray[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        optionsBtn.setTitle(findingSourceArray[row], for: .normal)
        optionsBtn.setTitleColor(UIColor.red, for: .normal)
        pickerView.isHidden = true
    }
    
    func updateClientObject() {
        client.aboutUs = "\(optionsBtn.titleLabel!.text)"
      client.description = descriptionTF.text
    }
    
    @IBAction func optionsBtnPressed(_ sender: Any) {
        pickerView.isHidden = false
    }
    
    
    @IBAction func backBtnPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func bookBtnPressed(_ sender: Any) {
        updateClientObject()
       
        bookAssesment()
    }
    
    func bookAssesment() {
        let clientLead : [String: String] = ["First Name" : client.fName, "Last Name" : client.lName, "Contact Number" : client.number, "Email" : client.email, "Query Type" : client.visaType, "Source" : client.aboutUs, "Description" : client.description]
        
        let databaseRef = Database.database().reference()
        databaseRef.child("New Leads").childByAutoId().setValue(clientLead)
    }
    
}
