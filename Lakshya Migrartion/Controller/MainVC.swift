//
//  ViewController.swift
//  Lakshya Migrartion
//
//  Created by Raunak Singh on 21/11/17.
//  Copyright © 2017 Raunak Singh. All rights reserved.
//

import UIKit

class MainVC: UIViewController {


    @IBOutlet weak var imageView: UIImageView!
    
    let imagesArray: [UIImage] = [UIImage (imageLiteralResourceName: "StudentVisa"), UIImage (imageLiteralResourceName: "PartnerVisa"), UIImage (imageLiteralResourceName: "Visa457"), UIImage (imageLiteralResourceName: "SkilledMigration")]
    let visaTypeArray:[String] = ["","Student Visa", "Skilled Migration","Course Change","Employer Sponsorship","Partner Visa","485 Visa"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        animateImages()
        
    }

    @IBAction func VisaButtonPressed(sender: UIButton) {
        print(sender.tag)
        let visa: VisaClass = VisaClass(vt: visaTypeArray[sender.tag], vd: "", vl: "")
        
        print(visa.visaType)
        performSegue(withIdentifier: "showVisaType", sender: visa)
    }
    
    func animateImages() {
        imageView.animationImages = self.imagesArray
        imageView.animationDuration = 10
        imageView.startAnimating()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showVisaType" {
            if let visaTypeVC = segue.destination as? VisaTypeVC {
                if let visa1 = sender as? VisaClass {
                    visaTypeVC.visa = visa1
                }
            }
        }
    }
}

