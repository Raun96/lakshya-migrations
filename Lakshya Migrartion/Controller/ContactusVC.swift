//
//  ContactusVC.swift
//  Lakshya Migrartion
//
//  Created by Raunak Singh on 16/12/17.
//  Copyright © 2017 Raunak Singh. All rights reserved.
//

import UIKit
import MessageUI



class ContactusVC: UIViewController, MFMailComposeViewControllerDelegate {
   
    @IBOutlet weak var mailView: UIView!
    @IBOutlet weak var callusView: UIView!
    var mailVC: MFMailComposeViewController = MFMailComposeViewController()
    let toRecipients = ["info@lakshyamigrations.com.au"]
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mailVC.mailComposeDelegate = self
       
        roundedviews()
    }


    @IBAction func backBtnPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func sendMail(_ sender: Any) {
        mailVC.setToRecipients(toRecipients)
        
        self.present(mailVC, animated: true, completion: nil)
        
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        switch result {
        case .sent:
            dismiss(animated: true, completion: nil)
        case .saved:
            dismiss(animated: true, completion: nil)
        case .cancelled:
            dismiss(animated: true, completion: nil)
        case .failed:
            mailError()
        default:
            print(error!)
        }
    }
    
    func mailError() {                                                      // For the mail alert.
        let alert = UIAlertController(title: "Can't send email", message: "Your device can't send email at the moment. Try again later.", preferredStyle: .actionSheet)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func callBtnPressed(_ sender: Any) {
                                                                            // To make a call.
        if let phoneCallURL = URL(string: "tel://+610433840304)") {
            
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                application.open(phoneCallURL, options: [:], completionHandler: nil)
         
    }
        }
        
    }
    
    @IBAction func vistWebsiteBtnPressed(_sender: Any) {
        let websiteURL: URL = URL(string: "http://www.lakshyamigrations.com.au")!
        UIApplication.shared.open(websiteURL, options: [:], completionHandler: nil)
    }
    @IBAction func shareBtnPressed(_ sender: Any) {
     
        let linktoShare = ""
        let textToShare = "Hey guys,\nCheck out this very informative and useful Migration helps that solves all your migration queries. Download their app from the app store to get your first consultation for free. \n\(linktoShare)"
        let ActivityVC = UIActivityViewController(activityItems: [textToShare], applicationActivities: nil)
        
        ActivityVC.popoverPresentationController?.sourceView = sender as? UIView
        self.present(ActivityVC, animated: true, completion: nil)
        
    }
    
    func roundedviews() {
        self.mailView.layer.cornerRadius = 8.0
        self.callusView.layer.cornerRadius = 8.0
    }
    
}
