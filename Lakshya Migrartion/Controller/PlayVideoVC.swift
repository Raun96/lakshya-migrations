//
//  PlayVideoVC.swift
//  Lakshya Migrartion
//
//  Created by Raunak Singh on 28/12/17.
//  Copyright © 2017 Raunak Singh. All rights reserved.
//

import UIKit
import YouTubePlayer

class PlayVideoVC: UIViewController {

    var vid: videos!
    
   @IBOutlet weak var videoView: YouTubePlayerView!
    @IBOutlet weak var videoTitle: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var videoDesc: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        print(vid)
        self.UpdateUI()
    }


    @IBAction func backButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func UpdateUI() {
      
        self.videoTitle.text = self.vid.videoTitle
        let videoURL = URL(string: self.vid.videoURL)
        self.videoView.loadVideoURL(videoURL!)
        self.videoDesc.text = self.vid.description
        self.dateLbl.text = self.vid.date
        
    }
    
    @IBAction func shareBtnPressed(_ sender: Any) {
        let linkToShare: String = self.vid.videoURL
        let textToShare: String = "Hi Guys, \nCheck out this very informative video on \(self.vid.videoTitle) by Lakshya Migrations. Download their app from the app store. \n"
        let activityVC = UIActivityViewController(activityItems: [textToShare,linkToShare], applicationActivities: nil)
        
        print(textToShare, linkToShare)
        activityVC.popoverPresentationController?.sourceView = sender as! UIView
        self.present(activityVC, animated: true, completion: nil)
        
    }
    
}
