//
//  VisaTypeVC.swift
//  Lakshya Migrartion
//
//  Created by Raunak Singh on 10/12/17.
//  Copyright © 2017 Raunak Singh. All rights reserved.
//

import UIKit

class VisaTypeVC: UIViewController {

    @IBOutlet weak var visaTypeLabel: UILabel!
    var visa: VisaClass!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateUI()
       
    }

    @IBAction func backBtnPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func updateUI() {
        visaTypeLabel.text = visa.visaType
        
    }
}
