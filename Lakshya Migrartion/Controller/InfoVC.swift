//
//  InfoVC.swift
//  Lakshya Migrartion
//
//  Created by Raunak Singh on 1/1/18.
//  Copyright © 2018 Raunak Singh. All rights reserved.
//

import UIKit

class InfoVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        appinfoView.layer.cornerRadius = 8.0
        
    }

    
    @IBAction func backBtnPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBOutlet weak var appinfoView: UIView!
}
