//
//  UpdatesVC.swift
//  Lakshya Migrartion
//
//  Created by Raunak Singh on 7/1/18.
//  Copyright © 2018 Raunak Singh. All rights reserved.
//

import UIKit
import Alamofire

class UpdatesVC: UIViewController,UITableViewDelegate, UITableViewDataSource {

    
    @IBOutlet weak var tableview: UITableView!
    var updatesArray = [Updates]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        EZLoadingActivity.show("Loading Updates...", disableUI: true)

        tableview.dataSource = self
        tableview.delegate = self
    
        
        self.downloadNewsUpdates {}
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return updatesArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
         if let cell = tableView.dequeueReusableCell(withIdentifier: "updatesCell", for: indexPath) as? newsUpdateViewCell {
            let update = updatesArray[indexPath.row]
            cell.updateUI(update: update)
            return cell
         } else {
            return UITableViewCell()
        }
        }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let updateLink: URL = URL(string: updatesArray[indexPath.row].link)!
        print("Clicked the update..............\(updateLink)")
        UIApplication.shared.open(updateLink, options: [:], completionHandler: nil)
    }
    
    func downloadNewsUpdates(completed: downloadComplete) {
        let newsURL = URL(string: jsonNewsUpdatesURL)!
        Alamofire.request(newsURL).responseJSON { Result in
        
            if let dict = Result.value as? Dictionary<String, AnyObject> {
                if let newsUpdate = dict["NewsUpdate"] as? [Dictionary<String,String>] {
                    for obj in newsUpdate {
                        let title: String = obj["Title"]!
                        let updateLink: String = obj["Link"]!
                        let update = Updates(title: title, link: updateLink)
                        self.updatesArray.append(update)
                    }
                }
                EZLoadingActivity.hide()
                self.tableview.reloadData()
            }
        }
        completed()
        
    }
    

 

  
    @IBAction func backBtnPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
